# Script db

```postgres-sql`
CREATE TABLE user_times
(
id uuid NOT NULL, user_id uuid NOT NULL, time timestamp NOT NULL, type integer NOT NULL,

    CONSTRAINT times_id PRIMARY KEY (id),
    CONSTRAINT times_id_fkey FOREIGN KEY (user_id) REFERENCES users (id)

);

CREATE TABLE users
(
id uuid NOT NULL, name varchar NOT NULL, password varchar NOT NULL,

    CONSTRAINT users_id PRIMARY KEY (id)

);

create extension "uuid-ossp";

-- password1: 123, password2: 321 insert into users values (uuid_generate_v4(), 'dima', '
$2a$10$PrI5Gk9L.tSZiW9FXhTS8O8Mz9E97k2FZbFvGFFaSsiTUIl.TCrFu'); insert into users values (uuid_generate_v4(), 'sasha', '
$2a$12$4PxzvxepMoM7nFVyjaAgZeariYyk41LkQ5SMjkrsb3W6Q3HC/OdyW');

```