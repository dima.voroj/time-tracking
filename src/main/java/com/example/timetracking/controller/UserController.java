package com.example.timetracking.controller;

import com.example.timetracking.service.UserService;
import com.example.timetracking.util.TimeType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestParam;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@RestController
@RequestMapping
public class UserController {
    private final UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping
    public ResponseEntity<?> login(HttpServletResponse response) throws IOException {
        response.sendRedirect("/action.html");
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping("/action")
    public ResponseEntity<?> start(@RequestParam String type, HttpServletResponse response) throws IOException {
        userService.addRecord(TimeType.valueOf(type));
        response.sendRedirect("/action.html");
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping("/report")
    public ResponseEntity<?> report() {
        String report = userService.report();
        return new ResponseEntity<>(report, HttpStatus.OK);
    }
}
