package com.example.timetracking.model;

import com.example.timetracking.util.TimeType;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.OffsetDateTime;
import java.util.UUID;

@Entity
@Table(name = "user_times")
@Getter
@Setter
@NoArgsConstructor
public class UserTime {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private UUID id;

    @Column(name = "time")
    private OffsetDateTime dateTime;

    @Enumerated(EnumType.ORDINAL)
    private TimeType type;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;
}
