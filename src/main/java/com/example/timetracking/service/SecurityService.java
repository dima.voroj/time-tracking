package com.example.timetracking.service;

public interface SecurityService {
    String findLoggedInUsername();
}
