package com.example.timetracking.service;

import com.example.timetracking.util.TimeType;

public interface UserService {
    void addRecord(TimeType type);

    String report();
}
