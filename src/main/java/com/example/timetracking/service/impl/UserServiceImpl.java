package com.example.timetracking.service.impl;

import com.example.timetracking.model.User;
import com.example.timetracking.model.UserTime;
import com.example.timetracking.repository.UserRepository;
import com.example.timetracking.repository.UserTimeRepository;
import com.example.timetracking.service.SecurityService;
import com.example.timetracking.service.UserService;
import com.example.timetracking.util.TimeType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

@Service
public class UserServiceImpl implements UserService {
    private final UserRepository userRepository;
    private final SecurityService securityService;
    private final UserTimeRepository userTimeRepository;

    @Autowired
    public UserServiceImpl(UserRepository userRepository, SecurityService securityService, UserTimeRepository userTimeRepository) {
        this.userRepository = userRepository;
        this.securityService = securityService;
        this.userTimeRepository = userTimeRepository;
    }

    @Override
    public String report() {
        String loggedInUsername = securityService.findLoggedInUsername();
        User byUsername = userRepository.findByUsername(loggedInUsername);
        List<UserTime> userTimes = byUsername.getUserTimes().stream().filter(d -> d.getDateTime().isAfter(OffsetDateTime.of(LocalDate.now(),
                LocalTime.MIDNIGHT, ZoneOffset.UTC))).collect(Collectors.toList());

        long millis = 0;
        for (int i = 0; i < userTimes.size(); i += 2) {
            if (i == userTimes.size() - 1) {
                if (i + 1 >= userTimes.size()) {
                    millis += (OffsetDateTime.now().toInstant().toEpochMilli() -
                            userTimes.get(userTimes.size() - 1).getDateTime().toInstant().toEpochMilli());
                } else {
                    millis += (userTimes.get(userTimes.size() - 2).getDateTime().toInstant().toEpochMilli() -
                            userTimes.get(userTimes.size() - 1).getDateTime().toInstant().toEpochMilli());
                }
            } else {
                millis += (userTimes.get(i + 1).getDateTime().toInstant().toEpochMilli() -
                        userTimes.get(i).getDateTime().toInstant().toEpochMilli());
            }
        }

        String hms = String.format("%02d:%02d:%02d", TimeUnit.MILLISECONDS.toHours(millis),
                TimeUnit.MILLISECONDS.toMinutes(millis) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millis)),
                TimeUnit.MILLISECONDS.toSeconds(millis) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));

        return userTimes.stream().filter(d -> d.getDateTime().isAfter(OffsetDateTime.of(LocalDate.now(),
                LocalTime.MIDNIGHT, ZoneOffset.UTC)))
                .map(u -> u.getDateTime() + " - date-type: " + u.getType() + "<br>").collect(Collectors.joining()) + "Total: " + hms;
    }

    public void addRecord(TimeType type) {
        String loggedInUsername = securityService.findLoggedInUsername();
        User byUsername = userRepository.findByUsername(loggedInUsername);
        List<UserTime> userTimes = byUsername.getUserTimes();

        if (userTimes.isEmpty() || userTimes.get(userTimes.size() - 1).getType() != type) {
            UserTime userTime = new UserTime();
            userTime.setDateTime(OffsetDateTime.now());
            userTime.setType(type);
            userTime.setUser(byUsername);
            userTimes.add(userTime);
            userTimeRepository.save(userTime);
            byUsername.setUserTimes(userTimes);
            userRepository.save(byUsername);
        }
    }
}
